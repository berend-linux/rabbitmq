#!/usr/bin/env python
# Author: Berend de Groot
# Webservers RabbitMQ message broker reciever

import pika
import subprocess


credentials = pika.PlainCredentials('nugtr', 'nugtr8080');
connection = pika.BlockingConnection(pika.ConnectionParameters('fds01','5672', '/', credentials))
channel = connection.channel()
channel.exchange_declare(exchange='webservers', exchange_type='fanout')

result = channel.queue_declare(queue='')
queue_name = result.method.queue;
channel.queue_bind(exchange='webservers', queue=queue_name)


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

    if body == 'apache_reload':
        command = ['service', 'apache2', 'reload']

    if body == 'apache_restart':
	command = ['service', 'apache2', 'restart']

    if body == 'nginx_reload':
       command = ['service', 'nginx', 'reload'];

    if body == 'nginx_restart':
       command = ['service', 'nginx', 'restart'];

    if body == 'php-fpm_reload':
       command = ['service', 'php7.4-fpm', 'reload'];

    if body == 'php-fpm_reload':
       command = ['service', 'php7.4-fpm', 'reload'];

    if 'command' in locals():
        subprocess.call(command, shell=False)

channel.basic_consume(queue=queue_name, auto_ack=True, on_message_callback=callback)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
