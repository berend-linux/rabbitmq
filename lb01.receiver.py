#!/usr/bin/env python
# Author: Berend de Groot
# Loadbalancer RabbitMQ message broker receiver

import pika
import subprocess


credentials = pika.PlainCredentials('nugtr', 'nugtr8080');
connection = pika.BlockingConnection(pika.ConnectionParameters('fds01','5672', '/', credentials))
channel = connection.channel()

channel.queue_declare(queue='webservers')


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

    if body == 'haproxy_reload':
        command = ['service', 'haproxy', 'reload']

    if body == 'haproxy_restart':
	command = ['service', 'haproxy', 'restart']

    if body == 'fail2ban_reload':
        command = ['service', 'fail2ban', 'reload'];

    if body == 'fail2ban_restart':
        command = ['service', 'fail2ban', 'restart'];

    if 'command' in locals():
        subprocess.call(command, shell=False)

channel.basic_consume(queue='webservers',auto_ack=True,on_message_callback=callback)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
