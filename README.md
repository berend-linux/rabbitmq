## RabbitMQ Message Broker
I use RabbitMQ to share messges between servers. More info:

- https://www.rabbitmq.com/tutorials/tutorial-one-python.html
- https://www.rabbitmq.com/install-debian.html#erlang-source-list-file

For more detailed information check our server network design documentation.

## Small note
For now I added commands to reload/restart haproxy, fail2ban, nginx, apache & PHP FPM.
It is not yet possible to send any command, perhaps that is something for the future!

## RabbitMQ Service
Our python listener is registered as service and starts on boot, in case you need to reproduce:
- Move the file rabbitmq.service to /lib/systemd/system/rabbitmq.service
- Change permissions to 644 (chmod 644/lib/systemd/system/rabbitmq.service)
- Reload daemon - systemctl daemon-reload
- Enable startup service - systemctl enable rabbitmq.service
- Profit! 

## Detaching python script
The python script listening for messages should always run on the server, I will make sure it runs on boot but in case it does not you can start it with this command: 
- nohup /websites/config/rabbitmw/web0x.receiver.py & (or whereever you put the script) 
You can detach the window with Ctrl + C and the script wil keep running in the background. 
Type "ps aux | grep python" to check if it is running:
- root 28778 0.0 0.4 100552 17368 pts/0 S 13:04 0:00 python /websites/config/rabbitmq/web0x.receiver.py
