#!/usr/bin/env python
# Author Berend de Groot 
# RabbitMQ Message Broker sender

import pika
import sys

action = sys.argv[1]

credentials = pika.PlainCredentials('nugtr', 'nugtr8080');
connection = pika.BlockingConnection(pika.ConnectionParameters('fds01','5672', '/', credentials))

channel = connection.channel()

channel.exchange_declare(exchange='webservers', exchange_type='fanout')

channel.basic_publish(exchange='webservers',routing_key='',body=action)

print("send {} accross network!".format(action))
connection.close();
